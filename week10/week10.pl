use strict;
use warnings;


my $filename = @ARGB[0];

open IN, "<" , $filename or die "Can not open $filename: $!" ;

my @lines = <IN>;

close IN;


foreach my $line (@lines) {

	chomp $line;
	my @cols = split(";" , $line);
	
	print "$cols[1]\t$cols[4]\t$cols[5]\n"

}
