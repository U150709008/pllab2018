use strict;
use warnings;

print "Hellooo, this is my first perl script!!! :) \n";

my $name_of_variable = 5;

print 'The value of $name_of_variable is :' . "$name_of_variable" . "\n";

# '  ' means it prints whatever it sees! :) 
# "  " prints , its value instead of its name 


$name_of_variable = " This time im assigning a STRING! :) " ;

print 'The value of $name_of_variable is :' . "$name_of_variable" . "\n";

my @array = (10,20,30) ;

print "@array \n" ;

# in an array you can store same type of and different type of variables 

@array = (10, "This is a STRING!" , 5.9, $name_of_variable) ;

print "@array \n" ;
