#use strict;
use warnings;

my @arr = ( "one", "two", "Three" , "Heyo!") ;
my $size = @arr;	 #bir arrayi scalardeğere atarsan, sana doğrudan size'ını döndürür.
print $size, "\n" ; 	#Prints 3

my ( $a,$b,$c,$d ) = @arr; 	# forth one will be undefined
print "$a $b $c $d\n";



my $DNA1= "ATG";
my $DNA2= "CCC";
my $DNA3= $DNA1 . $DNA2; 	# concatenation operator
$DNA3= "$DNA1$DNA2"; 	# string interpolation
print "$DNA3 \n"; # prints ATGCCC




my @arr2 = qw/This is just a sentence! :). /   ;

print "@arr2 \n" ;

print "$arr2[2] \n" ;

#2li bi array (2 dimentional (array of array)) olsaydı @ kullanırdık ama bu tekli!



print "@{$matrix[2]} \n" ;
